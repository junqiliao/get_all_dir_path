import os

# 指定文件夹路径
folder_path = 'D:\\liaojq\\ljq\\LBH100\\LBH100_Program\\Z_LBH100_20240606\\LBH100_BMU_BASE\\code'


def list_paths(folder_path, separator='/', include_files=False, include_hidden=False):
    paths = []
    for root, dirs, files in os.walk(folder_path):
        # 检查并跳过隐藏目录（如果选项指定）
        dirs_to_keep = [d for d in dirs if not d.startswith('.') or include_hidden]
        for d in list(dirs):  # 先读取一次，以免迭代时修改dirs列表
            if d.startswith('.') and not include_hidden:
                dirs.remove(d)

        # 添加目录和文件的绝对路径（如果include_files为真）
        for dirpath in dirs_to_keep:
            dir_abspath = os.path.join(root, dirpath)
            paths.append(os.path.abspath(dir_abspath).replace(os.sep, separator))

        if include_files:
            for file in files:
                file_path = os.path.join(root, file)
                paths.append(os.path.abspath(file_path).replace(os.sep, separator))
    return paths


def save_to_text_file(paths, output_file):
    with open(output_file, 'w', encoding='utf-8') as f:
        for path in paths:
            f.write(path + '\n')


# 获取用户输入
separator = None
ret = input("请选择路径分隔符（0: '/' 或 1: '\\\\'）：")  # 用户选择路径分隔符
if ret == '0':
    separator = '/'  # 用户选择路径分隔符
elif ret == '1':
    separator = '\\\\'

# 根据用户输入校验分隔符
if separator not in ['/', '\\\\']:
    print("错误的分隔符选择，请选择'/'或'\\\\'。")
else:
    include_files = input("是否需要包括文件的绝对路径？(y/n)：").lower() == 'y'
    include_hidden = input("是否包含隐藏文件夹？(y/n)：").lower() == 'y'

    # 创建路径列表并保存到文本文件
    paths = list_paths(folder_path, separator, include_files, include_hidden)
    output_file = 'folder_paths.txt'
    save_to_text_file(paths, output_file)
    print(f"路径列表已经保存到 {output_file}")